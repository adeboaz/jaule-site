// Abrir menu
$("#open-menu").click(function(event){
  event.preventDefault();
  $('body').toggleClass('menu--active');
});

if($(window).width()<=1023){
  $(".navbar .menu a").click(function(event){
    event.preventDefault();
    $('body').removeClass('menu--active');
  });
}

// Close menu mobile on click
if($(window).width()<=1023){
  $(".navbar a").click(function(event){
    event.preventDefault();
    $('body').removeClass('menu--active');
  });
}

// Modal
$('.frm-control .frm-help .open-modal').bind('click', function(event){
	event.preventDefault();
	$(this).parents('.frm-control .frm-help').find('.modal').fadeIn(500);
	$("body").addClass('modal-active');
});

$('.modal-close').bind('click', function(event){
	event.preventDefault();
	$(".modal").fadeOut();
	$("body").removeClass('modal-active');
});

// Carousel de introdução
var swiper = new Swiper(".swiper-intro", {
  effect: "fade",
  speed: 600,
  spaceBetween: 30,
  loop: true,
  slidesPerView: 1,
  autoplay: {
    delay: 15000,
    disableOnInteraction: true,
  },
  pagination: {
    el: ".swiper-pagination-intro",
    clickable: true,
  },
});

// Carousel clientes
var swiper = new Swiper(".swiper-clientes", {
  slidesPerView: 2,
  spaceBetween: 30,
  navigation: {
    nextEl: ".swiper-clientes-next",
    prevEl: ".swiper-clientes-prev",
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 48,
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 56,
    },
  },
});

// Link âncora
$(document).on('click', '.menu a[href^="#"]', function (event) {
  event.preventDefault();

  $('html, body').animate({
      scrollTop: $($.attr(this, 'href')).offset().top - 120
  }, 500);
});

// Accordion
$(function() {
	
	$(".accordion_tab").on("click", function(e) {
		e.preventDefault();
		var $this = $(this);
		if (!$this.hasClass("active")) {
			$(".accordion_content").slideUp(300);
			$(".accordion_tab").removeClass("active");
		}
		$this.toggleClass("active");
		$this.next().slideToggle(300);
	});
	
});

// Parallax
if($(window).width()>=1023){
  var rellax = new Rellax('.rellax', {
    center: true
  });
}

// Animacoes
AOS.init({
  disable: 'mobile',
  duration: 1000,
  easing: 'ease-in-out',
});

// Lottie
if($(window).width()>=1023){
  LottieInteractivity.create({
    mode: 'scroll',
    player: '#rede-de-energia',
    actions: [
      {
        visibility: [0, 1],
        type: 'seek',
        frames: [0, 85],
      },
    ],
  });
} 