// Abrir menu
$("#open-menu").click(function(event){
  event.preventDefault();
  $('body').toggleClass('menu--active');
});

if($(window).width()<=1023){
  $(".navbar .menu a").click(function(event){
    event.preventDefault();
    $('body').removeClass('menu--active');
  });
}

// Close menu mobile on click
if($(window).width()<=1023){
  $(".navbar a").click(function(event){
    event.preventDefault();
    $('body').removeClass('menu--active');
  });
}

// Modal
$('.frm-control .frm-help .open-modal').bind('click', function(event){
	event.preventDefault();
	$(this).parents('.frm-control .frm-help').find('.modal').fadeIn(500);
	$("body").addClass('modal-active');
});

$('.modal-close').bind('click', function(event){
	event.preventDefault();
	$(".modal").fadeOut();
	$("body").removeClass('modal-active');
});

// Mask
jQuery(document).ready(function ($) {
  $('.masc-data').mask('00/00/0000');
  $('.masc-cep').mask('00000-000');
  $('.masc-telefone').mask('(00) 0000-0000');
  $('.masc-celular').mask('(00) 0 0000-0000');
  $('.masc-cpf').mask('000.000.000-00', { reverse: true });
  $('.masc-cnpj').mask('00.000.000/0000-00', { reverse: true });
  $('.masc-dinheiro').mask('000.000.000.000.000,00', { reverse: true });
});

// Carousel clientes
var swiper = new Swiper(".swiper-clientes", {
  slidesPerView: 2,
  spaceBetween: 30,
  navigation: {
    nextEl: ".swiper-clientes-next",
    prevEl: ".swiper-clientes-prev",
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 48,
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 56,
    },
  },
});

// Link âncora
$(document).on('click', 'a[href^="#"]', function (event) {
  event.preventDefault();

  $('html, body').animate({
      scrollTop: $($.attr(this, 'href')).offset().top - 120
  }, 500);
});