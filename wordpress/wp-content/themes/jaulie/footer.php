<footer class="footer footer--main">
  <div class="container">

    <?php if (is_page_template('landing-page.php')): ?>
    <?php if( have_rows('proposta') ): ?>
    <?php while( have_rows('proposta') ): the_row(); ?>
      <?php include( get_template_directory().'/inc/landing-page/proposta.php') ?>
      <div class="simulador-regulamentado">
        <p>
          <svg class="icon icon-security"><use xlink:href="#icon-security"></use></svg>
          Modelo regulamentado
        </p>
        <img src="<?php echo get_template_directory_uri(); ?>/source/img/marcas/aneel-white.svg" alt="">
      </div>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php else: ?>
      <div class="sign">
        <img src="<?php echo get_template_directory_uri(); ?>/source/img/marcas/jaulie-simbolo.svg" alt="">
        <div class="vstack gap--2">
          <h2><?php the_field('titulo_principal_rodape', 'option'); ?></h2>
          <h3><?php the_field('subtitulo_rodape', 'option'); ?></h3>
        </div>
      </div>
    <?php endif ?>

    <span class="divider"></span>
    <div class="footer-contacts">
      <p class="copyright"><?php the_field('copyright_rodape', 'option'); ?></p>
      <ul class="footer-links hstack gap--4">
        <li>
          <a href="mailto:<?php the_field('email_rodape', 'option'); ?>" title="Envie uma mensagem para <?php the_field('email_rodape', 'option'); ?>" class="email-item">
            <svg class="icon icon-email"><use xlink:href="#icon-email"></use></svg>
            <span><?php the_field('email_rodape', 'option'); ?></span>
          </a>
        </li>
        <li>
          <a href="tel:<?php the_field('telefone_rodape', 'option'); ?>" title="Envie-nos uma mensagem" class="phone-item">
            <svg class="icon icon-telefone"><use xlink:href="#icon-telefone"></use></svg>
            <span><?php the_field('telefone_rodape', 'option'); ?></span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</footer>

<?php if (is_page_template('landing-page.php')): ?>
<?php else: ?>
  <div data-tf-popover="JrQ7oy6b" data-tf-button-color="#00BF47" data-tf-tooltip="Olá! 👋 Posso te ajudar?" data-tf-iframe-props="title=Jaulie, o futuro da energia" style="all:unset;"></div>
  <script src="//embed.typeform.com/next/embed.js"></script>
<?php endif ?>

<?php if (is_page_template('landing-page.php')): ?>
  <?php the_field('js_footer_code'); ?>
  <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/swiper-bundle.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mask.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/app-lp.js"></script>
<?php else: ?>
  <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/swiper-bundle.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/rellax.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/aos.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/lottie-player.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/lottie-interactivity.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/app.js"></script>
<?php endif ?>

<?php wp_footer(); ?>

</body>
</html>