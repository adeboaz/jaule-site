<?php /* Template Name: Blank */ ?>
<!DOCTYPE html>
<html lang="pt-br" class="no-js">
<head itemscope itemtype="http://schema.org/WebSite">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title itemprop="name"><?php wp_title('');?></title>
  <meta itemprop="name" content="<?php wp_title(' ');?>">
  <meta itemprop="description" content="<?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true); ?>">
  <meta itemprop="image" content="https://geracao.solar/wp-content/uploads/2021/12/jaulie-opengraph.jpg">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700&amp;display=swap" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/style.min.css" rel="stylesheet" media="screen">
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="js/support-ie.js"></script>
  <![endif]-->
  <style type="text/css">

    .card-simulador {
      border: 2px solid black;
      min-height: 600px;
      padding: 2rem;
      display: flex;
      flex-direction: column;
      align-items: center;
    }

    .card-simulador {

    }

    /* Card Solicitar Proposta */
    .card-solicitar-proposta {
      display: none;
    }

    .card-simulador.card-solicitar-proposta--active .card-solicitar-proposta {
      display: block;
    }

    .card-simulador.card-solicitar-proposta--active .card-simulador-form {
      display: none;
    }

    /* Card Repetir Simulação */
    .card-repetir-simulacao {
      display: none;
    }

    .card-simulador.card-repetir-simulacao--active .card-repetir-simulacao {
      display: block;
    }

    .card-simulador.card-repetir-simulacao--active .card-simulador-form {
      display: none;
    }

  </style>

  </style>
  <?php wp_head(); ?>
</head>
<body>
<?php if(have_posts()) : the_post();  ;?>
<div class="container">

  <section>
    <h1><?php echo the_title(); ?></h1>
  </section>

  <div class="spacing"></div>

  <section>
    <h2>Veja quanto você pode economizar com a energia verde</h2>

    <div class="card-simulador">

      <!-- Card Simulador -->
      <div id="card-form" class="card-simulador-form">
        <?php echo do_shortcode('[contact-form-7 id="253" title="Simulador"]'); ?>
      </div>
      <!-- Card Simulador -->

      <!-- Card Solicitar Proposta -->
      <div id="card-success" class="card-solicitar-proposta">
        <p>Tenha até <span id="success-percentage">N%</span> de desconto na sua anuidade</p>
        <p>E até <span id="success-value">R$ 50</span> de desconto por ano na sua conta de energia</p>
        <a href="#proposta" title="Solicitar Proposta"><strong>Solicitar Proposta</strong></a>
        <br>
        <a id="new-solicitation" href="#" title="Realizar nova simulação">Realizar nova simulação</a>
      </div>
      <!-- Card Solicitar Proposta -->

      <!-- Card Solicitar Proposta -->
      <div id="card-fail" class="card-repetir-simulacao">
        <p>Infelizmente não há disponibilidade para atender o seu perfil de consumo.</p>
        <p>Tem uma conta com consumo maior? Faça uma nova simulação.</p>
        <a id="new-solicitation-2" href="#" title="Nova Simulação"><strong>Nova Simulação</strong></a>
      </div>
      <!-- Card Solicitar Proposta -->

    </div>

  </section>

  <div class="spacing"></div>

  <section id="proposta">
    <h2>Solicitar Proposta</h2>
    <div>
      <?php echo do_shortcode('[contact-form-7 id="254" title="Solicitar Proposta"]'); ?>
    </div>
  </section>

  <div class="spacing"></div>

</div>
<?php endif; ?>

<script>
    (function() {
        const annuityIpt = document.getElementById("anuidade-oab-simulador");
        const consumptionIpt = document.getElementById("consumo-medio-simulador");
        const resultIpt = document.getElementById("resultado-consumo-medio-simulador");
        const resultProposalIpt = document.getElementById("resultado-consumo-medio-proposta");
        const newSolicitationBtn = document.getElementById("new-solicitation");
        const newSolicitation2Btn = document.getElementById("new-solicitation-2");
        const registrationIpt = document.getElementById("inscricao-oab-simulador");
        const registrationProposalIpt = document.getElementById("inscricao-oab-proposta");
        const whatsappIpt = document.getElementById("whatsapp-simulador");
        const whatsappProposalIpt = document.getElementById("whatsapp-proposta");

        function calculateN() {
          const annuity = parseFloat(annuityIpt.value) || 0;
          const consumption = parseFloat(consumptionIpt.value) || 0;

          return (((consumption - 150) * 0.84) / annuity) * 100;
        }

        function updateResultField() {
          const n = calculateN();
          resultIpt.value = n;
        }

        annuityIpt.addEventListener("change", updateResultField);
        consumptionIpt.addEventListener("change", updateResultField);

        newSolicitationBtn.addEventListener("click", function() {
          document.getElementById("card-success").style.display = "none";
          document.getElementById("card-fail").style.display = "none";
          document.getElementById("card-form").style.display = "block";
        });

        newSolicitation2Btn.addEventListener("click", function() {
          document.getElementById("card-success").style.display = "none";
          document.getElementById("card-fail").style.display = "none";
          document.getElementById("card-form").style.display = "block";
        });

        document.addEventListener( 'wpcf7mailsent', function( event ) {
          document.getElementById("card-form").style.display = "none";

          const consumption = parseFloat(consumptionIpt.value) || 0;

          if (consumption >= 300) {
            const n = calculateN();
            resultProposalIpt.value = n;
            registrationProposalIpt.value = registrationIpt.value;
            whatsappProposalIpt.value = whatsappIpt.value;
            document.getElementById("success-percentage")
              .innerHTML = `${n}%`.replace('.', ',');
            document.getElementById("success-value")
              .innerHTML = `R$ ${((consumption - 150)*1.2).toFixed(2)}`.replace('.', ',');
            document.getElementById("card-success").style.display = "block";
          } else {
            document.getElementById("card-fail").style.display = "block";
          }

//           reset();
        }, false );
    })();
</script>


<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
