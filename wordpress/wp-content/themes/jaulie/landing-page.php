<?php /* Template Name: Landing Page */ ?>
<?php get_header(); ?>

<main class="main-wrapper">
  <?php include( get_template_directory().'/inc/landing-page/intro.php') ?>
  <?php include( get_template_directory().'/inc/landing-page/simulador.php') ?>
  <?php include( get_template_directory().'/inc/landing-page/clientes.php') ?>
</main>

<?php get_footer(); ?>