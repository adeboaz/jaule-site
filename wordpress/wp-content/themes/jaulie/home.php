<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<main class="main-wrapper">
  <?php include( get_template_directory().'/inc/home/intro.php') ?>
  <?php include( get_template_directory().'/inc/home/como-funciona.php') ?>
  <?php include( get_template_directory().'/inc/home/o-que-fazemos.php') ?>
  <?php include( get_template_directory().'/inc/home/quero-fazer-parte.php') ?>
  <?php include( get_template_directory().'/inc/home/perguntas-frequentes.php') ?>
  <?php include( get_template_directory().'/inc/home/clientes.php') ?>
</main>
<?php get_footer(); ?>