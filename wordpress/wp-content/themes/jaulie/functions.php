<?php

function setup(){

	register_nav_menus( array( 'main_menu' => __('Menu Principal') ) );
	register_nav_menus( array( 'lp_menu' => __('Menu LP') ) );
	register_nav_menus( array( 'actions_menu' => __('Menu de ações') ) );
	add_theme_support('post-thumbnails');

}

add_action('after_setup_theme', 'setup');

if( function_exists('acf_add_options_page') ) {	
	acf_add_options_page();
}