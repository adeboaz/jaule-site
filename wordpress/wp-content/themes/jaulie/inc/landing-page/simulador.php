<section class="como-funciona" id="simulador">
<div class="container">
    
    <div class="spacing"></div>
    <div class="spacing-content"></div>

    <div class="col-960" style="margin: 0 auto">
      <h2 class="heading--2" style="text-align: center"><?php the_field('titulo_simulador'); ?></h2>
    </div>

    <div class="spacing-content"></div>

  <div class="card-simulador">

    <!-- Card Simulador -->
    <?php if( have_rows('simulador') ): ?>
    <?php while( have_rows('simulador') ): the_row(); ?>
    <div class="frm">
      <div id="card-form" class="card-simulador-form">
        <?php the_sub_field('conteudo'); ?>
      </div>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>
    <!-- Card Simulador -->

    <!-- Card Solicitar Proposta -->
    <?php if( have_rows('simulador_sucesso') ): ?>
    <?php while( have_rows('simulador_sucesso') ): the_row(); ?>
    <div id="card-success" class="card-solicitar-proposta">
      <div class="content">
        <div class="card-body">
          <img src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/simulador-sucesso.svg">
          <?php the_sub_field('resultado_content'); ?>
        </div>
        <div class="card-actions">
          <?php if( have_rows('acoes') ): ?>
          <?php while( have_rows('acoes') ): the_row(); ?>  
            <?php the_sub_field('botoes'); ?>
          <?php endwhile; ?>
          <?php endif; ?>
        </div>
        <?php if( have_rows('simulador') ): ?>
        <?php while( have_rows('simulador') ): the_row(); ?>
        <div class="card-footer">
          <?php the_sub_field('rodape'); ?>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>
    <!-- Card Solicitar Proposta -->

    <!-- Card Solicitar Proposta -->
    <?php if( have_rows('simulador_erro') ): ?>
    <?php while( have_rows('simulador_erro') ): the_row(); ?>
    <div id="card-fail" class="card-repetir-simulacao">
      <div class="content">
        <img src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/repetir-simulacao.svg">
        <?php the_sub_field('conteudo'); ?>
        <div class="card-actions">
          <?php the_sub_field('acoes'); ?>
        </div>
        <?php if( have_rows('simulador') ): ?>
        <?php while( have_rows('simulador') ): the_row(); ?>
        <div class="card-footer">
          <?php the_sub_field('rodape'); ?>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>
    <!-- Card Solicitar Proposta -->

    <div class="simulador-regulamentado">
      <p>
        <svg class="icon icon-security"><use xlink:href="#icon-security"></use></svg>
        Modelo regulamentado
      </p>
      <img src="<?php echo get_template_directory_uri(); ?>/source/img/marcas/aneel.svg" alt="">
    </div>

  </div>
  <div class="spacing"></div>
</div>
</section>