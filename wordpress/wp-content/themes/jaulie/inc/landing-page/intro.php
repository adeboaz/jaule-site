<?php if( have_rows('hero_landing_page') ): ?>
<?php while( have_rows('hero_landing_page') ): the_row(); ?>
<section class="intro-2">
  
  <div class="intro-inner">
    <div class="container">
      <div class="content vstack gap--5">
        <h2 class="heading--2"><?php the_sub_field('titulo'); ?></h2>
        <?php if( have_rows('vantagens') ): ?>
        <ul class="list-vantagens">
          <?php while( have_rows('vantagens') ): the_row(); ?>
          <li>
            <?php 
            $image = get_sub_field('imagem');
            if( !empty( $image ) ): ?>
            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
            <?php endif; ?>
            <h3><?php the_sub_field('titulo'); ?></h3>
          </li>
          <?php endwhile; ?>
        </ul>
        <?php endif; ?>

        <a class="button button--secondary" href="<?php the_sub_field('cta_url'); ?>" title="<?php the_sub_field('cta_label'); ?>">
          <?php the_sub_field('cta_label'); ?>
        </a>
        
        <?php if ( get_sub_field( 'content_footer_hero' ) ): ?>
        <div class="content-footer">
          <?php the_sub_field('content_footer_hero'); ?>
        </div>
        <?php else: ?>
        <?php endif; ?>

      </div>
    </div>
  </div>

  <div class="image"><img src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/mude-para-energia-verde.svg" alt="Mude para Energia Verde"></div>
  <div class="sun-shape-container"><div class="sun-shape-inner"><div class="sun-shape-2"></div></div>

  </div>
</section>
<?php endwhile; ?>
<?php endif; ?>