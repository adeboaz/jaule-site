<div class="solicitar-proposta" id="proposta">
  <div class="col-570">
    <div class="sign">
      <img src="<?php echo get_template_directory_uri(); ?>/source/img/marcas/jaulie-simbolo.svg" alt="">
      <div class="vstack gap--2">
        <?php the_sub_field('texto'); ?>
      </div>
    </div>
  </div>
  <div class="col-570">
    <div class="frm">
      <?php the_sub_field('formulario'); ?>
    </div>
  </div>
</div>