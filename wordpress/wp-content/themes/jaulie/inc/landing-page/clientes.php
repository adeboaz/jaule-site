<section class="clientes bg-secondary--500" id="clientes">
  <div class="container">
    
    <?php if( have_rows('clientes_landing_page') ): ?>
    <?php while( have_rows('clientes_landing_page') ): the_row(); ?>
    <div class="vstack gap--2 col-570">
      <span class="tagline color-primary--500"><?php the_sub_field('tagline'); ?></span>
      <h2 class="heading--3"><?php the_sub_field('titulo'); ?></h2>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>

    <div class="spacing-content"></div>
    <?php $the_query = new WP_Query( 'page_id=8' ); ?>
    <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
    <?php if( have_rows('clientes_home') ): ?>
    <?php while( have_rows('clientes_home') ): the_row(); ?>
    <?php $images = get_sub_field('clientes_galeria'); if( $images ): ?>
    <div class="swiper swiper-clientes">
      <div class="swiper-wrapper">
        <?php foreach( $images as $image ): ?>
        <div class="swiper-slide"><img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"/></div>
        <?php endforeach; ?>
      </div>
      <div class="swiper-navigation swiper-navigation--2">
        <div class="swiper-navigation-container">
          <div class="swiper-button-next swiper-clientes-next">
            <svg class="icon icon-arrow-right">
              <use xlink:href="#icon-arrow-right"></use>
            </svg>
          </div>
          <div class="swiper-button-prev swiper-clientes-prev">
            <svg class="icon icon-arrow-left">
              <use xlink:href="#icon-arrow-left"></use>
            </svg>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php endwhile;?>
    <?php wp_reset_query(); ?>

  </div>
</section>