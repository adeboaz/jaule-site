<?php if( have_rows('como_funciona_home') ): ?>
<?php while( have_rows('como_funciona_home') ): the_row(); ?>

<section class="como-funciona" id="como-funciona">
  <div class="container">

    <div class="spacing"></div>

    <?php if( have_rows('introducao') ): ?>
    <?php while( have_rows('introducao') ): the_row(); ?>
    <div class="vstack gap--2 col-768" data-aos="fade-up">
      <span class="tagline color-primary--500"><?php the_sub_field('tagline_introducao'); ?></span>
      <h2 class="heading--3"><?php the_sub_field('titulo_introducao'); ?></h2>
    </div>

    <div class="spacing"></div>

    <div data-aos="fade-up">
      <h2 class="heading--1"><?php the_sub_field('titulo_principal_introducao'); ?></h2>
    </div>

    <?php endwhile; ?>
    <?php endif; ?>

    <div class="spacing"></div>
    
    <?php if( have_rows('lista_como_funciona_home') ): ?>
    <div class="passo-a-passo">
      <?php while( have_rows('lista_como_funciona_home') ): the_row(); ?>  
      <div class="item" data-aos="fade-up">
        <div class="content vstack gap--4"><span class="tagline color-primary--500"><?php the_sub_field('tagline_lista_como_funciona'); ?></span>
          <h2 class="heading--4"><?php the_sub_field('titulo_lista_como_funciona'); ?></h2>
        </div>
        <?php if ( get_sub_field( 'lottie_url_lista_como_funciona' ) ): ?>
        <div class="image">
          <lottie-player id="rede-de-energia" src="<?php the_sub_field('lottie_url_lista_como_funciona'); ?>" style="height: auto; max-width: 800px;">"></lottie-player>
        </div>
        <?php else: ?>
          <div class="image"><img src="<?php the_sub_field('imagem_lista_como_funciona'); ?>" alt="<?php the_sub_field('tagline_lista_como_funciona'); ?>" /></div>
        <?php endif; ?>
      </div>
      <?php endwhile; ?>
    </div>
    <?php endif; ?>

  </div>
</section>

<?php endwhile; ?>
<?php endif; ?>