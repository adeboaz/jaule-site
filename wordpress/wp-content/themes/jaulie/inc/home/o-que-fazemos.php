<?php if( have_rows('o_que_fazemos_home') ): ?>
<?php while( have_rows('o_que_fazemos_home') ): the_row(); ?>

<section class="o-que-fazemos" id="o-que-fazemos">
  <div class="container">
    
    <?php if( have_rows('introducao') ): ?>
    <?php while( have_rows('introducao') ): the_row(); ?>
    <div class="vstack gap--2 col-768" data-aos="fade-up">
      <span class="tagline color-tertiary--500"><?php the_sub_field('tagline'); ?></span>
      <h2 class="heading--3"><?php the_sub_field('titulo'); ?></h2>
    </div>
    <div class="spacing"></div>
    <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('lista_o_que_fazemos') ): ?>
    <ul class="list-oque-fazemos" data-aos="fade-up">
      <?php while( have_rows('lista_o_que_fazemos') ): the_row(); ?>  
      <li class="vstack gap--5">
        <div class="thumbnail"><img src="<?php the_sub_field('imagem_lista_o_que_fazemos'); ?>" alt="<?php the_sub_field('titulo_lista_o_que_fazemos'); ?>" /></div>
        <h2 class="color-tertiary--500"><?php the_sub_field('titulo_lista_o_que_fazemos'); ?></h2>
        <div class="body-text">
          <p><?php the_sub_field('texto_lista_o_que_fazemos'); ?></p>
        </div>
      </li>
      <?php endwhile; ?>
    </ul>
    <?php endif; ?>

    <div class="spacing"></div>
    
    <?php if( have_rows('cta') ): ?>
    <?php while( have_rows('cta') ): the_row(); ?>
    <div class="vstack gap--10 col-768" data-aos="fade-up">
      <h2 class="heading--1"><?php the_sub_field('titulo'); ?></h2>
      <!-- <a class="button button--secondary" href="<?php // the_sub_field('url_botao_cta'); ?>" title="<?php // the_sub_field('label_botao_cta'); ?>" rel="noopener noreferrer" target="_blank" ><?php // the_sub_field('label_botao_cta'); ?></a> -->
      <a class="button button--secondary" data-tf-popup="JrQ7oy6b" data-tf-iframe-props="title=Jaulie, o futuro da energia" rel="noopener" title="<?php the_sub_field('label_botao_cta'); ?>"><?php the_sub_field('label_botao_cta'); ?></a>
    </div>
    <?php endwhile; ?>
    <?php endif; ?>

  </div>
  
  <div class="grafismo grafismo--1" data-aos="fade-in"><img src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/half-shape-blue.svg" alt="" /></div>
  <div class="grafismo grafismo--2" data-aos="fade-in"><img class="rellax" src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/half-shape-blue-blur.svg" alt="" data-rellax-speed="2" /></div>
  <div class="grafismo grafismo--3" data-aos="fade-in"><img class="rellax" src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/sol.svg" alt="" data-rellax-speed="2" /></div>
  <div class="grafismo grafismo--4" data-aos="fade-in"><img src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/shape-blue-top.svg" alt="" /></div>

</section>

<?php endwhile; ?>
<?php endif; ?>