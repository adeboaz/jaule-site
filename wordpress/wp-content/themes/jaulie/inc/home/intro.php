<?php if( have_rows('slider_home') ): $i = 0; ?>
<section class="intro">
  <div class="swiper swiper-intro">
    <div class="swiper-wrapper">
    <?php while( have_rows('slider_home') ): the_row(); $i++;?>  
      <div class="swiper-slide item-<?php echo $i; ?>">
        <div class="container">
          <div class="content vstack gap--5"><span class="tagline color-secondary--500"><?php the_sub_field('tagline_slider_home'); ?></span>
            <h2 class="heading--2"><?php the_sub_field('titulo_slider_home'); ?></h2>
            <div class="body-text">
              <?php the_sub_field('texto_slider_home'); ?>
            </div>
            <!-- <a class="button button--secondary" href="<?php // the_sub_field('url_botao_slider_home'); ?>" title="<?php // the_sub_field('label_botao_slider_home'); ?>" rel="noopener noreferrer" target="_blank" ><?php // the_sub_field('label_botao_slider_home'); ?></a> -->
            <a class="button button--secondary" data-tf-popup="JrQ7oy6b" data-tf-iframe-props="title=Jaulie, o futuro da energia" rel="noopener" title="<?php the_sub_field('label_botao_cta'); ?>"><?php the_sub_field('label_botao_slider_home'); ?></a>
          </div>
        </div>
        <div class="container swiper-pagination-inner">
          <div class="swiper-pagination swiper-pagination-intro"></div>
        </div>
        <div class="image-svg">
          <div class="image-inner">
            <?php the_sub_field('svg_slider_home'); ?>
          </div>
        </div>
      </div>
      <?php endwhile; ?>
    </div>
  </div>
  <div class="sun-shape"></div>
</section>
<?php endif; ?>