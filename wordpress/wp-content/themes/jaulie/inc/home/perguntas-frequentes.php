<?php if( have_rows('perguntas_frequentes_home') ): ?>
<?php while( have_rows('perguntas_frequentes_home') ): the_row(); ?>

<section class="perguntas-frequentes" id="perguntas-frequentes">
  
  <div class="spacing"></div>
  
  <?php if( have_rows('introducao') ): ?>
  <?php while( have_rows('introducao') ): the_row(); ?>
  <section>
    <div class="container">
      <div class="vstack gap--2 col-570" data-aos="fade-up">
        <span class="tagline color-primary--500"><?php the_sub_field('tagline'); ?></span>
        <h2 class="heading--3"><?php the_sub_field('titulo'); ?></h2>
      </div>
    </div>
  </section>
  <?php endwhile; ?>
  <?php endif; ?>
  
  <div class="spacing"></div>
  
  <?php if( have_rows('lista-faq-home') ): ?>
  <section class="perguntas-frequentes-accordion">
    <div class="container">
      <div class="accordion-container col-570" data-aos="fade-up">
        <?php while( have_rows('lista-faq-home') ): the_row(); ?>  
        <div class="accordion">
          <div class="accordion_tab">
            <svg width="14" height="14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#a)" fill="#623FAF">
                <path d="M13 6H1a1 1 0 0 0 0 2h12a1 1 0 1 0 0-2Z"></path>
                <path id="minus" d="M7 0a1 1 0 0 0-1 1v12a1 1 0 1 0 2 0V1a1 1 0 0 0-1-1Z"></path>
              </g>
              <defs><clipPath id="a"><path fill="#fff" d="M0 0h14v14H0z"></path></clipPath></defs>
            </svg>
            <h2 class="heading--underline"><?php the_sub_field('titulo'); ?></h2>
          </div>
          <div class="accordion_content">
            <div class="accordion_item body-text vstack gap--6">
              <?php the_sub_field('texto'); ?>
            </div>
          </div>
        </div>
        <?php endwhile; ?>
      </div>

      <div class="spacing-content"></div>

      <?php if( have_rows('cta') ): ?>
      <?php while( have_rows('cta') ): the_row(); ?>
      <div class="col-570 vstack gap--4 mais-perguntas" data-aos="fade-up">
        <h2 class="heading--3 color-primary--500"><?php the_sub_field('titulo'); ?></h2>
        <div class="body-text">
          <?php the_sub_field('texto'); ?>
        </div>
      </div>
      <?php endwhile; ?>
      <?php endif; ?>

    </div>
    <div class="grafismo grafismo--2" data-aos="fade-in"><img class="rellax" src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/shape-purple-top.svg" alt="" data-rellax-speed="-2"/></div>
  </section>
  <?php endif; ?>

  <div class="spacing"></div>
  <div class="grafismo grafismo--1" data-aos="fade-in"><img class="rellax" src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/half-shape-purple-blur.svg" alt="" data-rellax-speed="2"/></div>

</section>

<?php endwhile; ?>
<?php endif; ?>