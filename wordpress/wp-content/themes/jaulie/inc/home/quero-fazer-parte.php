<?php if( have_rows('fazer_parte_home') ): ?>
<?php while( have_rows('fazer_parte_home') ): the_row(); ?>
<section class="quero-fazer-parte" id="quero-fazer-parte">
  <div class="container">
    <div class="vstack gap--10 col-570" data-aos="fade-right">
      <h2 class="heading--2 color-primary--500"><?php the_sub_field('titulo'); ?></h2>
      <!-- <a class="button button--primary" href="<?php // the_sub_field('url_do_botao'); ?>" title="<?php // the_sub_field('label_do_botao'); ?>" rel="noopener noreferrer" target="_blank"><?php // the_sub_field('label_do_botao'); ?></a> -->
      <a class="button button--primary" data-tf-popup="JrQ7oy6b" data-tf-iframe-props="title=Jaulie, o futuro da energia" rel="noopener" title="<?php the_sub_field('label_botao_cta'); ?>"><?php the_sub_field('label_do_botao'); ?></a>
    </div>
    <div class="spacing"></div>
    <div class="body-text vstack gap--6 col-460" data-aos="fade-left">
      <?php the_sub_field('texto'); ?>
    </div>
  </div>
  <div class="grafismo grafismo--1" data-aos="fade-in"><img src="<?php echo get_template_directory_uri(); ?>/source/img/ilustracoes/half-shape-purple.svg" alt="" /></div>
</section>
<?php endwhile; ?>
<?php endif; ?>