<!DOCTYPE html>
<html lang="pt-br" class="no-js">

<head itemscope itemtype="http://schema.org/WebSite">
  
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="google-site-verification" content="wGpljMk_nlrFG25Xk5owtQH8ZYulJjIcfuVC3DKigzQ" />
  <title itemprop="name"><?php wp_title(' ');?></title>
  
  <!-- Google / Search Engine Tags -->
  <meta itemprop="name" content="<?php wp_title(' ');?>">
  <meta itemprop="description" content="<?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true); ?>">
  <meta itemprop="image" content="https://geracao.solar/wp-content/uploads/2021/12/jaulie-opengraph.jpg">
  
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700&amp;display=swap" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/style.min.css" rel="stylesheet" media="screen">
  <?php include( get_template_directory().'/inc/head/favicons.php') ?>
  <?php include( get_template_directory().'/inc/head/opengraph.php') ?>
  <?php include( get_template_directory().'/inc/head/analyticstracking.php') ?>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="js/support-ie.js"></script>
  <![endif]-->

  <?php if (is_page_template('landing-page.php')): ?>
    <?php the_field('js_head_code'); ?>
  <?php else: ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TM8M2NM');</script>
    <!-- End Google Tag Manager -->
  <?php endif ?>

  <?php wp_head(); ?>
</head>

<body id="inicio">

  <?php if (is_page_template('landing-page.php')): ?>
    <?php the_field('js_body_code'); ?>
  <?php else: ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TM8M2NM"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  <?php endif ?>

  <?php include( get_template_directory().'/source/icon/svg/symbols.svg') ?>
  <header class="header header--main">
    <div class="container">
      <h1 class="logo">
        <a href="#inicio" title="Jaulie"><svg class="icon icon-jaulie"><use xlink:href="#icon-jaulie"></use></svg></a>
      </h1>
      <div id="open-menu">
        <a class="button-icon" href="#" title="Menu">
          <svg class="icon icon-menu"><use xlink:href="#icon-menu"></use></svg>
          <svg class="icon icon-close"><use xlink:href="#icon-close"></use></svg>
        </a>
      </div>
      <div class="navbar">
        <nav class="menu">
          <?php if (is_page_template('landing-page.php')): ?>
            <?php if (has_nav_menu('lp_menu')) : wp_nav_menu(array('theme_location'  => 'lp_menu')); endif; ?>
          <?php else: ?>
            <?php if (has_nav_menu('main_menu')) : wp_nav_menu(array('theme_location'  => 'main_menu')); endif; ?>
          <?php endif ?>
        </nav>
        
        <?php if (is_page_template('landing-page.php')): ?>
          <div class="menu-acoes-container">
            <ul id="menu-acoes" class="actions">
              <li id="menu-item-98" class="button menu-item menu-item-type-custom menu-item-object-custom menu-item-98">
                <a href="#proposta" title="Solicitar Proposta">Solicitar Proposta</a>
              </li>
            </ul>
          </div>
        <?php else: ?>
          <div class="menu-acoes-container">
            <ul id="menu-acoes" class="actions">
              <li id="menu-item-98" class="button menu-item menu-item-type-custom menu-item-object-custom menu-item-98">
                <a data-tf-popup="JrQ7oy6b" data-tf-iframe-props="title=Jaulie, o futuro da energia" rel="noopener" title="<?php the_sub_field('label_botao_cta'); ?>">Quero economizar</a>
              </li>
            </ul>
          </div>
        <?php endif ?>
          
      </div>
    </div>
  </header>